#! /usr/bin/env python3

# The stereo_image_proc pipeline assumes that every /image_raw will have
# a corresponding /camera_info. Our bag files were uncalibrated, so I can
# either re-write them to have the appropriate files, or just spoof them.

import yaml

import rospy

import sensor_msgs.msg


def load_params(filename):
    """
    Load camera calibration parameters from a yaml file in the format produced by
    the image_pipeline.
    Return a CameraInfo message with the calibration fields filled out.
    """
    with open(filename, "r") as fp:
        calib = yaml.safe_load(fp)
    msg = sensor_msgs.msg.CameraInfo()
    msg.width = calib["image_width"]
    msg.height = calib["image_height"]
    msg.K = calib["camera_matrix"]["data"]
    msg.D = calib["distortion_coefficients"]["data"]
    msg.R = calib["rectification_matrix"]["data"]
    msg.P = calib["projection_matrix"]["data"]
    msg.distortion_model = calib["camera_model"]
    return msg


class CalibrationSpoofer(object):
    LEFT = 0
    RIGHT = 1

    def __init__(
        self, left_image_topic, right_image_topic, left_param_file, right_param_file
    ):
        self.image_topics = {}
        self.image_topics[self.LEFT] = left_image_topic
        self.image_topics[self.RIGHT] = right_image_topic

        self.param_files = {}
        self.param_files[self.LEFT] = left_param_file
        self.param_files[self.RIGHT] = right_param_file

        self.info_topics = {}
        self.subscribers = {}
        self.publishers = {}
        self.camera_info = {}
        for camera in [self.LEFT, self.RIGHT]:
            self.info_topics[camera] = self.image_topics[camera].replace(
                "image_raw", "camera_info"
            )
            cb = (
                lambda msg, self=self, camera=camera: self.image_callback(  # noqa: E731
                    msg, camera
                )
            )
            self.subscribers[camera] = rospy.Subscriber(
                self.image_topics[camera], sensor_msgs.msg.Image, cb, queue_size=1
            )
            self.publishers[camera] = rospy.Publisher(
                self.info_topics[camera], sensor_msgs.msg.CameraInfo, queue_size=1
            )
            self.camera_info[camera] = load_params(self.param_files[camera])

    def image_callback(self, msg, camera):
        # print("Got image from the {} camera".format(camera))
        self.camera_info[camera].header = msg.header
        self.publishers[camera].publish(self.camera_info[camera])


if __name__ == "__main__":
    rospy.init_node("camera_calibration_spoofer")
    # The bagfiles that Aaron collected use the topics /left/image_raw and /right/image_raw
    left_filename = rospy.get_param("~left_stereo_filename")
    right_filename = rospy.get_param("~right_stereo_filename")
    spoofer = CalibrationSpoofer(
        "/left/image_raw", "/right/image_raw", left_filename, right_filename
    )
    rospy.spin()
