# Author: Laura Lindzey
# Copyright 2020-2022 University of Washington Applied Physics Laboratory
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.

# Plotting utilities used by CameraCalibrationPipeline.ipynb
import matplotlib.pyplot as plt
import numpy

import cv2


def plot_aruco_markers(aruco_dictionary, fig_dir, nrows, ncols):
    # type: (cv2.aruco_Dictionary, cv2.aruco_CharucoBoard, string, int, int) -> None
    """
    Plot the aruco dictionary used, as well as the generated board layout.
    * fig_dir - where to save resulting figures
    """
    # NB: For the 4x4 dictionary, valid marker indices are 0..249, so this doesn't
    #     show all of them.
    fig_rows = nrows
    fig_cols = ncols
    # need some additional vertical space for titles
    fig = plt.figure(figsize=[15, 1.1 * nrows * (15.0 / ncols)])
    axs = fig.subplots(fig_rows, fig_cols)
    for ii in range(fig_rows):
        for jj in range(fig_cols):
            idx = ii * fig_cols + jj
            marker = aruco_dictionary.drawMarker(idx, 250)
            axs[ii][jj].set_title("{}".format(idx))
            axs[ii][jj].imshow(marker, cmap="gray")
            axs[ii][jj].axis("off")

    if fig_dir is None:
        filename = "aruco_markers.png"
    else:
        filename = "{}/aruco_4x4_markers.png".format(fig_dir)
    plt.savefig(filename, bbox_inches="tight")
    plt.close()


def plot_charuco_board(charuco_board, fig_dir):
    board_img = charuco_board.draw((1100, 800))
    fig = plt.figure(figsize=[6, 4])
    ax = fig.add_axes([0, 0, 1, 1])
    ax.imshow(board_img, cmap="gray", aspect="equal", interpolation="none")
    ax.axis("off")
    ax.set_title("Charuco Board")
    plt.savefig("{}/charuco_board.png".format(fig_dir), bbox_inches="tight")
    plt.close()


def plot_image_detections(frame, figname=None, show=False):
    # type: (Frame) -> None
    """
    Plot the detected markers, charuco corners, and board pose on the image.
    """
    annotated_image = cv2.imread(frame.image_filepath)

    # These were too small to easily see, so I wrote my own
    # Including IDs causes lines to be drawn across the image.
    # cv2.aruco.drawDetectedMarkers(annotated_image, frame.aruco_corners) #, marker_ids)
    # Including the IDs would lead to lines across the image, just like in drawDetectedMarkers
    # cv2.aruco.drawDetectedCornersCharuco(annotated_image, frame.charuco_corners) #, charuco_ids)

    fig = plt.figure(figsize=[10, 8])
    ax = fig.add_axes([0, 0, 1, 1])
    ax.imshow(annotated_image, cmap="gray")

    if len(frame.aruco_corners) > 0:
        for corner, corner_id in zip(frame.aruco_corners, frame.aruco_ids):
            pt1, pt2, pt3, pt4 = corner[0]
            xx, yy = zip(pt1, pt2, pt3, pt4, pt1)
            plt.plot(xx, yy, "c")
            plt.text(
                numpy.mean(xx), numpy.mean(yy), "{}".format(corner_id[0]), color="c"
            )

    # for corner in frame.rejected_points:
    #    pt1, pt2, pt3, pt4 = corner[0]
    #    xx, yy = zip(pt1, pt2, pt3, pt4, pt1)
    #    plt.plot(xx, yy, 'gray')

    if frame.charuco_corners is not None:
        for corner, corner_id in zip(frame.charuco_corners, frame.charuco_ids):
            xx, yy = corner[0]
            circle = plt.Circle((xx, yy), 5, color="r")
            ax.add_artist(circle)
            plt.text(xx + 10, yy + 10, "{}".format(corner_id[0]), color="r")

    if figname is not None:
        plt.savefig(figname, bbox_inches="tight")
        plt.close()

    if show:
        plt.show()


# Returns a figure so it can be explicitly closed to enable creating images in a loop
# and use pyplot w/in ipython notebook
def plot_pose_distributions(index, selected=None, filename=None):
    # type: (List[Frame], List[Tuple[str, List[int]]]) -> matplotlib.figure.Figure
    """
    Plot distributions of charuco board poses in the index.

    If selected is not None, plot subsets of the index in the specified colors.
    Fields are: (color, indices)
    """
    xx = numpy.array([frame.center_x for frame in index.values() if frame.is_valid])
    yy = numpy.array([frame.center_y for frame in index.values() if frame.is_valid])
    skew = numpy.array([frame.skew for frame in index.values() if frame.is_valid])
    area = numpy.array(
        [numpy.sqrt(frame.area) for frame in index.values() if frame.is_valid]
    )

    fig = plt.figure(figsize=(15, 10))
    axs = fig.subplots(2, 3)

    axs[0][0].plot(xx, yy, "k.")
    axs[0][0].set_xlabel("X")
    axs[0][0].set_ylabel("Y")

    axs[0][1].plot(xx, area, "k.")
    axs[0][1].set_xlabel("X")
    axs[0][1].set_ylabel("sqrt(area)")

    axs[0][2].plot(yy, area, "k.")
    axs[0][2].set_xlabel("Y")
    axs[0][2].set_ylabel("sqrt(area)")

    axs[1][0].plot(skew, area, "k.")
    axs[1][0].set_xlabel("skew")
    axs[1][0].set_ylabel("sqrt(area)")

    axs[1][1].plot(xx, skew, "k.")
    axs[1][1].set_xlabel("X")
    axs[1][1].set_ylabel("skew")

    axs[1][2].plot(yy, skew, "k.")
    axs[1][2].set_xlabel("Y")
    axs[1][2].set_ylabel("skew")

    if selected is not None:
        for color, seqs in selected:
            xx = numpy.array([index[seq].center_x for seq in seqs])
            yy = numpy.array([index[seq].center_y for seq in seqs])
            skew = numpy.array([index[seq].skew for seq in seqs])
            area = numpy.array([numpy.sqrt(index[seq].area) for seq in seqs])

            axs[0][0].plot(xx, yy, ".", color=color)
            axs[0][1].plot(xx, area, ".", color=color)
            axs[0][2].plot(yy, area, ".", color=color)
            axs[1][0].plot(skew, area, ".", color=color)
            axs[1][1].plot(xx, skew, ".", color=color)
            axs[1][2].plot(yy, skew, ".", color=color)

    if filename is not None:
        plt.savefig(filename, bbox_inches="tight")

    return fig


def plot_selected_images(index, seqs, filename=None):
    # type: (Dict[int, Frame], List[int]) -> matplotlib.figure.Figure
    """
    Plots the images corresponding to input sequence numbers
    * index - map from sequence number to stats about the image
    * seqs - list of selected sequence numbers
    """
    ncols = 10
    nrows = numpy.ceil(1.0 * len(seqs) / ncols).astype(int)
    fig = plt.figure(figsize=[15, 1.1 * nrows * (15.0 / ncols)])

    # The default return types for subplots are obnoxious.
    # * if nrows == ncols == 1, AxesSubplot
    # * if nrows or ncols == 1, [AxesSubplot]
    # * otherwise, [[AxesSubplot]]
    # setting squeeze=False suppresses this behavior.
    axs = fig.subplots(nrows, ncols, squeeze=False)

    # Turn off axis for ALL subplots, even the unused ones
    for ax_row in axs:
        for ax in ax_row:
            ax.axis("off")

    for idx, seq in enumerate(seqs):
        row = numpy.floor(1.0 * idx / ncols).astype(int)
        col = idx % ncols
        image = cv2.imread(index[seq].image_filepath)
        axs[row][col].imshow(image)
        axs[row][col].set_title("Img {}".format(seq))

    if filename is not None:
        plt.savefig(filename, bbox_inches="tight")

    return fig
