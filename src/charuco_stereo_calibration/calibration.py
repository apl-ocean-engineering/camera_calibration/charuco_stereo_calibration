# Author: Laura Lindzey
# Copyright 2020-2022 University of Washington Applied Physics Laboratory
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.

"""
Functions used by CameraCalibrationPipeline.ipynb and StereoCalibration.ipynb

(Extracted to a separate file because the notebook was getting unwieldy)

Some of this functionality was migrated to the charuco_utils package, since
they were needed for more than just calibration:
* whitelists for fiducial_ids and plinth_ids
* is_good_marker
* get_board_params
* get_charuco_board
"""

from __future__ import print_function
from future.utils import iteritems

from datetime import datetime

import cv2
import cv2.aruco
import glob
import math
import numpy as np
import os
import yaml

import rosbag
import cv_bridge

import charuco_utils


class Frame(object):
    def __init__(
        self,
        tt,
        image_filepath,
        aruco_corners,
        aruco_ids,
        rejected_points,
        charuco_corners,
        charuco_ids,
    ):
        """
        Struct used to store detected locations from a single image.

        """
        # Relative path to the corresponding image file
        self.image_filepath = image_filepath  # type: str

        # Timestamp from the images header
        self.tt = tt  # type: float

        if len(aruco_corners) == 0:
            assert aruco_ids is None
        else:
            assert len(aruco_corners) == len(aruco_ids)

        if charuco_corners is None:
            assert charuco_ids is None
        else:
            assert len(charuco_corners) == len(charuco_ids)

        # Each ndarray in this list specifies the 4 corners of an aruco marker
        # as a 4x2 ndarray in image coordinates.
        self.aruco_corners = aruco_corners  # type: List[numpy.ndarray]

        # Nx1 ndarray with the indices of the detected aruco markers.
        # A charuco board will typically use markers in row-major order,
        # starting with the top-left and ending bottom-right.
        self.aruco_ids = aruco_ids  # type: Optional[numpy.ndarray]

        # Possible markers that were rejected; supposedly useful for debug
        # Having plotted them, I don't think that they are a useful criteria
        # for rejecting images from the calibration set.
        self.rejected_points = rejected_points  # type: List[numpy.ndarray]

        # Nx1x2 ndarray: detected charuco corners in image coordinates
        self.charuco_corners = charuco_corners  # type: Optional[numpy.ndarray]

        # Nx1 ndarray with IDs for the detected charuco corners
        # Corners are *interior* corners, numbered in row-major order,
        # starting from the lower-left of the charuco board and ending
        # upper-right. This is opposite from the arrangement of the
        # markers by id.
        self.charuco_ids = charuco_ids  # type: Optional[numpy.ndarray]

        # Whether the charuco corners are valid.
        # NB: This is NOT the metric for whether this image is good enough
        #     to use in the calibration. It only indicates that none of the
        #     Optional fields are None.
        # TODO: It might be worth adding a check for duplicated aruco marker
        #     IDs here, rather than having that appear in the frame selection
        #     code.
        self.is_valid = len(self.aruco_corners) > 0 and self.charuco_corners is not None

        # Descriptions of the charuco board location in the image.
        # A good calibration should include images with a distribution of these metrics.

        # Average skew of all detected aruco markers
        self.skew = None  # type: Optional[float]
        # Average area of all detected aruco markers (related to board's
        # z-position, but not independent of skew)
        self.area = None  # type: Optional[float]

        self.center_x = None  # type: Optional[float]
        self.center_y = None  # type: Optional[float]

        if self.is_valid:
            corners = [elem[0] for elem in self.aruco_corners]
            self.skew = np.mean(list(map(_calculate_skew, corners)))
            self.area = np.mean(list(map(_calculate_area, corners)))

            # Average of charuco centers
            self.center_x = np.mean(self.charuco_corners[:, 0, 0])
            self.center_y = np.mean(self.charuco_corners[:, 0, 1])


def get_image_size(bag_directory, image_topic):
    bridge = cv_bridge.CvBridge()
    image_shape = None
    bag_filenames = os.listdir(bag_directory)
    for bag_filename in bag_filenames:
        if not bag_filename.endswith("bag"):
            continue
        bag_filepath = "{}/{}".format(bag_directory, bag_filename)
        bag = rosbag.Bag(bag_filepath)

        for _topic, msg, _tt in bag.read_messages(topics=[image_topic]):
            image = bridge.imgmsg_to_cv2(msg, "mono8")
            image_shape = image.shape
            break
        break
    # image_size is (cols, rows) while shape is (rows, cols)
    return (image_shape[1], image_shape[0])


def datetime_from_filename(imgfname):
    ymd = imgfname.split("T")[0]
    hms = imgfname.split("T")[1]
    year = ymd[0:4]
    month = ymd[4:6]
    day = ymd[6:8]
    hr = hms[0:2]
    m = hms[2:4]
    s = hms[4:6]
    ms = hms.split("_")[1].replace(".png", "")

    dt = datetime(
        year=int(year),
        month=int(month),
        day=int(day),
        hour=int(hr),
        minute=int(m),
        second=int(s),
        microsecond=int(ms),
    )

    return dt


def get_directory_images_utc(dirpath, encoding=".png"):
    """
    Takes in a path, and returns images ordered by UTC encoding.

    Specifically expects to be in form:
    YYYYMMDDThhmmss_micros
    For example, 20210119T002553_599000 = 021/01/19 at 00:25:53.599000 (UTC)
    """
    imgfnames = []
    fnames = glob.glob(dirpath + "*" + encoding)
    for fname in fnames:
        imgname = fname.split("/")[-1]
        dt = datetime_from_filename(imgname)

        imgfnames.append((dt, fname))

    imgfnames = sorted(imgfnames)

    return_fnames = [val[1] for val in imgfnames]

    return return_fnames


def get_directory_images(dirpath, encoding=".png"):
    """
    Takes in a path of images and orders based on string id
    """
    fnames = sorted(glob.glob(dirpath + "*" + encoding))

    return fnames


def generate_frame(image, charuco_board, aruco_dictionary, tt, fname):
    cc = charuco_utils.detect_charuco_corners(aruco_dictionary, charuco_board, image)
    (
        marker_corners,
        marker_ids,
        rejected_points,
        valid_charuco_corners,
        valid_charuco_ids,
    ) = cc

    frame = Frame(
        tt,
        fname,
        marker_corners,
        marker_ids,
        rejected_points,
        valid_charuco_corners,
        valid_charuco_ids,
    )
    return frame


def generate_image_index_directory(
    aruco_dictionary, charuco_board, image_directory, encoding=".png", order_utc=False
):
    # type: (cv2.aruco_Dictionary, cv2.charuco_Board, str, str) -> Dict[int, Frame]

    """
    Returns an index to the images, mapping sequence number to filename and
    locations of detected aruco markers and charuco corners.

    * Assumes input images are 8-bit.
    * Extracted images will be converted to mono8

    # TODO may want to have a color option for other proc stuff
    """
    index = {}
    if order_utc:
        fnames = get_directory_images_utc(image_directory, encoding=encoding)
    else:
        fnames = get_directory_images(image_directory, encoding=encoding)
    num_skipped_aruco = 0
    num_skipped_charuco = 0
    img_size = (-1, -1)

    for tt, fname in enumerate(fnames):
        print("Looking at frame {}".format(fname.split("/")[-1]))
        img = cv2.imread(fname)
        if len(img.shape) > 2:
            image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_size = (image.shape[1], image.shape[0])

        # Store all of the calculated metrics.
        # Don't have timestamp info this way, so need to assume synced...
        frame = generate_frame(image, charuco_board, aruco_dictionary, tt, fname)
        if len(frame.aruco_corners) == 0:
            num_skipped_aruco += 1
        elif frame.charuco_corners is None:
            num_skipped_charuco += 1
        index[tt] = frame

    print("Got {} total frames".format(len(index)))
    print(
        "{} images have no aruco markers, and {} more have no charuco corners".format(
            num_skipped_aruco, num_skipped_charuco
        )
    )
    return index, img_size


def generate_image_index(
    bag_directory,
    image_topic,
    aruco_dictionary,
    charuco_board,
    image_directory,
    save_images=False,
):
    # type: (str, str, cv2.aruco_Dictionary, cv2.charuco_Board, str) -> Dict[int, Frame]
    """
    Extract all images in the bagfile that were published on the input topic,
    and save as PNG in the destination directory.

    Returns an index to the images, mapping sequence number to filename and
    locations of detected aruco markers and charuco corners.

    * Assumes input images are 8-bit.
    * Extracted images are mono8, since that's what is used by the calibration
      routines.
    * Output image names are destination_directory/img_{%06d}.png
    """
    if save_images:
        if not os.path.exists(image_directory):
            os.makedirs(image_directory)
        if not os.path.isdir(image_directory):
            raise Exception("Failed to create directory: {}".format(image_directory))

    index = {}
    bridge = cv_bridge.CvBridge()
    num_skipped_aruco = 0
    num_skipped_charuco = 0

    bag_filenames = os.listdir(bag_directory)
    for bag_filename in bag_filenames:
        if not bag_filename.endswith("bag"):
            continue
        bag_filepath = "{}/{}".format(bag_directory, bag_filename)
        print("Processing bagfile: {}".format(bag_filepath))
        bag = rosbag.Bag(bag_filepath)

        for _topic, msg, tt in bag.read_messages(topics=[image_topic]):
            # Our encoding is bayer_rggb8, so it can be directly converted to mono8.
            # camera_calibration/calibrator.py uses a more general conversion
            # that checks for and handles 16bit and FC1 encodings.
            image = bridge.imgmsg_to_cv2(msg, "mono8")

            # Save image so we can load it later without having to scan
            # through the entire bag
            image_filepath = "{}/img_{:06d}.png".format(image_directory, msg.header.seq)
            if save_images:
                if not os.path.exists(image_filepath):
                    cv2.imwrite(image_filepath, image)

            frame = generate_frame(
                image, charuco_board, aruco_dictionary, tt.to_sec(), image_filepath
            )
            if len(frame.aruco_corners) == 0:
                num_skipped_aruco += 1
            elif frame.charuco_corners is None:
                num_skipped_charuco += 1
            index[msg.header.seq] = frame

        bag.close()

    print("Got {} total frames on topic {}".format(len(index), image_topic))
    print(
        "{} images have no aruco markers, and {} more have no charuco corners".format(
            num_skipped_aruco, num_skipped_charuco
        )
    )
    return index


def select_evenly_spaced(index, num_images, min_corners, bad_seqs):
    # type: (Dict[int, Frame], int, int, List[int]) -> List[int]
    """
    Choose images to use in calibration from the input index.
    The selected images will be evenly spaced after unsuitable images
    have been filtered out.

    * num_images: how many images to return
    * min_corners: minimum number of detected charuco corners to use.
                   min_corners > max(board_rows, board_cols) guarantees
                   nonlinearity of the points.
    * bad_seqs: list of sequence numbers known to correspond to problematic
                   images.

    Returns list of sequence numbers.
    """
    valid_images = np.array(
        [
            seq
            for seq, frame in iteritems(index)
            if frame.is_valid and len(frame.charuco_corners) > min_corners
            # If there are repeated aruco tags, bad things happen
            # (Maybe this belongs in Frame.__init__?)
            and len(frame.aruco_ids) == len(np.unique(frame.aruco_ids))
            and seq not in bad_seqs
        ]
    )
    # Get evenly-spaced indices into valid_images, including the first and last.
    spaced_subset = np.round(np.linspace(0, len(valid_images) - 1, num_images)).astype(
        int
    )

    return valid_images[spaced_subset]


def calibrate_mono(charuco_board, image_size, index, seqs, display=False, wait_key=0):
    """
    Finally, perform the calibration!
    """
    all_charuco_corners = []
    all_charuco_ids = []
    for seq in seqs:
        all_charuco_corners.append(index[seq].charuco_corners)
        all_charuco_ids.append(index[seq].charuco_ids)
    if all_charuco_ids == [] or all_charuco_corners == []:
        return False
    all_charuco_corners = np.array(all_charuco_corners)
    all_charuco_ids = np.array(all_charuco_ids)

    cols, rows = image_size
    # I have no idea how to estimate this a priori.
    focal_length = np.sqrt(cols**2 + rows**2)
    camera_matrix = np.array(
        [
            [focal_length, 0.0, 0.5 * cols],
            [0.0, focal_length, 0.5 * rows],
            [0.0, 0.0, 1.0],
        ]
    )
    distortion = np.array([0.0, 0.0, 0.0, 0.0, 0.0])
    output = cv2.aruco.calibrateCameraCharucoExtended(
        all_charuco_corners,
        all_charuco_ids,
        charuco_board,
        image_size,
        camera_matrix,
        distortion,
    )
    return output


def save_calibration_mono(
    filename, camera_name, distortion, intrinsics, image_size, write_yaml_v1=False
):
    # alpha is a "zoom" quantity. set to 0, all pixels in calibrated images are valid.
    # set to 1, all pixels in original image will be in calibrated image
    alpha = 0
    proj = np.zeros((3, 4), dtype=np.float64)
    proj[:, 0:3], _ = cv2.getOptimalNewCameraMatrix(
        intrinsics, distortion, image_size, alpha
    )
    rect = np.eye(3)

    save(
        filename,
        camera_name,
        distortion,
        intrinsics,
        proj,
        rect,
        image_size,
        write_yaml_v1=write_yaml_v1,
    )


def save_calibration_stereo(
    filename,
    camera_name,
    distortion,
    intrinsics,
    proj,
    rect,
    image_size,
    write_yaml_v1=False,
):
    # Right now, this is assuming a pinhole camera and 5 distortion parameters
    # Alternatively, could use "rational_polynomial"
    save(
        filename,
        camera_name,
        distortion,
        intrinsics,
        proj,
        rect,
        image_size,
        write_yaml_v1=write_yaml_v1,
    )


def save(
    filename,
    camera_name,
    distortion,
    intrinsics,
    proj,
    rect,
    image_size,
    write_yaml_v1=False,
):
    camera_model = "plumb_bob"
    if write_yaml_v1:
        dump_yaml_v1(
            filename,
            image_size,
            camera_name,
            intrinsics,
            camera_model,
            distortion,
            rect,
            proj,
        )
    else:
        assert len(distortion) == 5

        yaml_str = lryaml(
            camera_name, distortion, intrinsics, rect, proj, image_size, camera_model
        )
        with open(filename, "w") as fp:
            fp.write(yaml_str)


def save_perspective_mat(filename, perspective, write_yaml_v1=False):
    """
    Save the perspective matrix (i.e. the 'Q' matrix) for stereo reprojection
    to its own yaml file
    """
    if write_yaml_v1:
        f = cv2.FileStorage(filename, flags=1)
        dump_mat(f, "perspective", perspective)
    else:
        perspective_str = (
            np.array2string(
                perspective, precision=5, suppress_small=True, separator=","
            )
            .replace("[", "")
            .replace("]", "")
            .replace("\n", "\n        ")
        )
        perspective_str = "[" + perspective_str[4:-5] + "]"

        yaml_str = (
            "perspective:\n"
            + "    rows: 4\n"
            + "    cols: 4 \n"
            + "    data: "
            + perspective_str
        )
        with open(filename, "w") as fp:
            fp.write(yaml_str)


def save_rot_trans(filename, rot, trans):
    """
    Save the rotation and translation to their own yaml file
    """
    out = {"rotation": rot.tolist(), "translation": trans.tolist()}

    with open(filename, "w") as fp:
        yaml.dump(out, fp)


#####################################
# Stereo calibration utilities
def stereo_pairs_gen(left_index, right_index, max_dt=0.01):
    """
    Yield pairs of stereo images separated by no more than max_dt.
    """
    left_seqs = sorted(left_index.keys())
    right_seqs = sorted(right_index.keys())
    left_idx = 0
    right_idx = 0
    while True:
        if left_idx >= len(left_seqs) or right_idx >= len(right_seqs):
            break

        left_seq = left_seqs[left_idx]
        right_seq = right_seqs[right_idx]
        left_tt = left_index[left_seq].tt
        right_tt = right_index[right_seq].tt

        if np.abs(left_tt - right_tt) < max_dt:
            yield (left_seq, right_seq, left_index[left_seq], right_index[right_seq])
            left_idx += 1
            right_idx += 1
        elif left_tt > right_tt:
            right_idx += 1
        else:
            left_idx += 1


def is_valid_frame(frame, min_corners):
    """
    Check if frame is valid.

    One easy-to-check problem is if the same aruco tag has been identified
    more than once in time image.
    """
    return (
        frame.is_valid
        and len(frame.charuco_corners) > min_corners
        and len(frame.aruco_ids) == len(np.unique(frame.aruco_ids))
    )


def valid_pairs_gen(pairs_gen, min_corners):
    for lseq, rseq, left, right in pairs_gen:
        if is_valid_frame(left, min_corners) and is_valid_frame(right, min_corners):
            # In addition to checking each frame individually,
            # check that we have a large-enough set of overlapping observations
            overlapping_ids = np.intersect1d(left.charuco_ids, right.charuco_ids)
            if len(overlapping_ids) > min_corners:
                yield (lseq, rseq, left, right)


def select_stereo_evenly_spaced(
    left_index, right_index, num_images, min_corners, max_dt=0.01
):
    # type: (Dict[int, Frame], Dict[int, Frame], int, int, float) -> List[int], List[int]
    """
    Selects stereo pairs from the two input indices.
    NB: The cameras we use nominally run at 20Hz. The bag files we have
        aren't synchronized (yet), but a quick analysis had showed that
        the images were always with in 0.01 seconds of each other.
    """
    # valid_pairs = [
    #     pair
    #     for pair in valid_pairs_gen(
    #         stereo_pairs_gen(left_index, right_index, max_dt=0.01), min_corners
    #     )
    #  ]
    valid_pairs = list(
        valid_pairs_gen(
            stereo_pairs_gen(left_index, right_index, max_dt=0.01), min_corners
        )
    )
    spaced_subset = np.round(np.linspace(0, len(valid_pairs) - 1, num_images)).astype(
        int
    )
    selected_pairs = [valid_pairs[idx] for idx in spaced_subset]
    return [(lseq, rseq) for (lseq, rseq, _, _) in selected_pairs]


class CalibrationParams(object):
    """
    Utility class for loading and using the intrinsic calibration's output
    """

    def __init__(self, filename, write_yaml_v1=False):
        if write_yaml_v1:
            fs = cv2.FileStorage()
            fs.open(filename, cv2.FileStorage_READ)
            self.distortion = np.array(fs.getNode("distortion_coefficients").mat())
            self.camera_matrix = np.array(fs.getNode("camera_matrix").mat())
        else:
            params = yaml.safe_load(open(filename, "r"))
            self.distortion = np.array(params["distortion_coefficients"]["data"])
            self.camera_matrix = np.array(params["camera_matrix"]["data"]).reshape(
                (3, 3)
            )


def calibrate_stereo(
    charuco_board,
    stereo_seqs,
    image_size,
    left_index,
    right_index,
    left_params,
    right_params,
    left_filename=None,
    right_filename=None,
    perspective_filename=None,
    transform_filename=None,
    write_yaml_v1=False,
    do_rectify=True,
):
    object_points = []
    image_points_left = []
    image_points_right = []
    for left_seq, right_seq in stereo_seqs:
        left_ids = left_index[left_seq].charuco_ids
        left_corners = left_index[left_seq].charuco_corners
        right_ids = right_index[right_seq].charuco_ids
        right_corners = right_index[right_seq].charuco_corners
        both_ids = np.intersect1d(left_ids, right_ids)
        board_corners = [charuco_board.chessboardCorners[cid] for cid in both_ids]
        selected_left_corners = [
            pt for (cid, pt) in zip(left_ids, left_corners) if cid in both_ids
        ]
        selected_right_corners = [
            pt for (cid, pt) in zip(right_ids, right_corners) if cid in both_ids
        ]
        object_points.append(np.array(board_corners))
        image_points_left.append(np.array(selected_left_corners))
        image_points_right.append(np.array(selected_right_corners))
    object_points = np.array(object_points)
    image_points_left = np.array(image_points_left)
    image_points_right = np.array(image_points_right)

    # In theory, opencv can do the full joint calibration.
    # In practice, it is recommended to calibrate the cameras individually, then
    # solve for the stereo parameters.
    flags = cv2.CALIB_FIX_INTRINSIC

    # Not sure how to pick these values; using the ones found in the ROS node
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 1, 1e-5)

    output = cv2.stereoCalibrate(
        object_points,
        image_points_left,
        image_points_right,
        left_params.camera_matrix,
        left_params.distortion,
        right_params.camera_matrix,
        right_params.distortion,
        image_size,
        flags=flags,
        criteria=criteria,
    )
    reproj_error, _, _, _, _, rotation, translation, essential, fundamental = output

    if transform_filename is not None:
        save_rot_trans(transform_filename, rotation, translation)

    if do_rectify is False:
        return -1.0

    # stereoRectify, takes the output of a calibration and generates the
    # corresponding rectification and projection matrices for a stereo pair.

    foo = cv2.stereoRectify(
        left_params.camera_matrix,
        left_params.distortion,
        right_params.camera_matrix,
        right_params.distortion,
        image_size,
        rotation,
        translation,
        flags=cv2.CALIB_ZERO_DISPARITY,
    )
    r_left, r_right, p_left, p_right, perspective, roi1, roi2 = foo

    if left_filename is not None:
        save_calibration_stereo(
            left_filename,
            "stereo_left",
            left_params.distortion,
            left_params.camera_matrix,
            p_left,
            r_left,
            image_size,
            write_yaml_v1=write_yaml_v1,
        )
    if right_filename is not None:
        save_calibration_stereo(
            right_filename,
            "stereo_right",
            right_params.distortion,
            right_params.camera_matrix,
            p_right,
            r_right,
            image_size,
            write_yaml_v1=write_yaml_v1,
        )
    if perspective_filename is not None:
        save_perspective_mat(perspective_filename, perspective, write_yaml_v1=True)

    return reproj_error


#####################################
# These are the definitions from:
# camera_calibration/src/camera_calibration/calibrator.py


def _calculate_skew(corners):
    """
    Get skew for given checkerboard detection.
    Scaled to [0,1], which 0 = no skew, 1 = high skew
    Skew is proportional to the divergence of three outside corners from 90 degrees.
    """
    # TODO Using three nearby interior corners might be more robust, outside corners occasionally
    # get mis-detected
    up_left, up_right, down_right, _ = corners

    def angle(a, b, c):
        """
        Return angle between lines ab, bc
        """
        ab = a - b
        cb = c - b
        return math.acos(np.dot(ab, cb) / (np.linalg.norm(ab) * np.linalg.norm(cb)))

    skew = min(1.0, 2.0 * abs((math.pi / 2.0) - angle(up_left, up_right, down_right)))
    return skew


def _calculate_area(corners):
    """
    Get 2d image area of the detected checkerboard.
    The projected checkerboard is assumed to be a convex quadrilateral, and the area computed as
    |p X q|/2; see http://mathworld.wolfram.com/Quadrilateral.html.
    """
    (up_left, up_right, down_right, down_left) = corners
    a = up_right - up_left
    b = down_right - up_right
    c = down_left - down_right
    p = b + c
    q = a + b
    return abs(p[0] * q[1] - p[1] * q[0]) / 2.0


# Params are:
# name, distortion, intrinsics(camera_matrix), rectification, projection, size, camera_model
# * Intrinsics -- as reported by calibrateCamera
# * Distortion: as computed by calibrateCamera
# * Rectification - typical I(3x3) for mono; only matters for stereo
# * Projection for mono appears to be calculated as:
#         self.P = np.zeros((3, 4), dtype=numpy.float64)
#         ncm, _ = cv2.getOptimalNewCameraMatrix(self.intrinsics, self.distortion, self.size, a)
#         for j in range(3):
#             for i in range(3):
#                 self.P[j,i] = ncm[j, i]
# * Size: (width, height)
# * Camera Model: CAMERA_MODEL.PINHOLE, which becomes the string "plumb_bob" (or "rational_polynomial" if len(d) > 5)
def lryaml(name, d, k, r, p, size, cam_model):
    def format_mat(x, precision):
        return "[%s]" % (
            np.array2string(x, precision=precision, suppress_small=True, separator=", ")
            .replace("[", "")
            .replace("]", "")
            .replace("\n", "\n        ")
        )

    # dist_model = _get_dist_model(d, cam_model)
    dist_model = cam_model

    assert k.shape == (3, 3)
    assert r.shape == (3, 3)
    assert p.shape == (3, 4)
    calmessage = "\n".join(
        [
            "image_width: %d" % size[0],
            "image_height: %d" % size[1],
            "camera_name: " + name,
            "camera_matrix:",
            "  rows: 3",
            "  cols: 3",
            "  data: " + format_mat(k, 5),
            "distortion_model: " + dist_model,
            "distortion_coefficients:",
            "  rows: 1",
            "  cols: %d" % d.size,
            "  data: [%s]" % ", ".join("%8f" % x for x in d.flat),
            "rectification_matrix:",
            "  rows: 3",
            "  cols: 3",
            "  data: " + format_mat(r, 8),
            "projection_matrix:",
            "  rows: 3",
            "  cols: 4",
            "  data: " + format_mat(p, 5),
            "",
        ]
    )
    return calmessage


def dump_yaml_v1(
    filename, image_size, camera_name, intrinsics, camera_model, distortion, rect, proj
):
    f = cv2.FileStorage(filename, flags=1)
    dump_mat(f, "image_width", image_size[0])
    dump_mat(f, "image_height", image_size[1])
    dump_mat(f, "camera_name", camera_name)
    dump_mat(f, "camera_matrix", intrinsics)
    dump_mat(f, "camera_model", camera_model)
    dump_mat(f, "distortion_coefficients", distortion)
    dump_mat(f, "rectification_matrix", rect)
    dump_mat(f, "projection_matrix", proj)


def dump_mat(file_storage, name, data):
    file_storage.write(name, data)


# Obsolete functions that are here for backwards compatability
def is_good_marker(aruco_dict, idx):
    # type: (cv2.aruco_Dictionary, int) -> bool
    print(
        "charuco_stereo_calibration.is_good_marker: DO NOT USE IN NEW CODE; "
        "use charuco_utils's version instead."
    )
    return charuco_utils.is_good_marker(aruco_dict, idx)


def get_board_params(name):
    # type: String -> Tuple[int, int, float]
    print(
        "charuco_stereo_calibration.get_board_params: DO NOT USE IN NEW CODE; "
        "use charuco_utils's version instead."
    )
    return charuco_utils.get_board_params(name)


def get_charuco_board(name):
    # type: string -> Tuple[cv2.aruco_Dictionary, cv2.aruco_CharucoBoard]
    print(
        "charuco_stereo_calibration.get_charuco_board: DO NOT USE IN NEW CODE; "
        "use charuco_utils's version instead."
    )
    return charuco_utils.get_charuco_board(name)


def deduplicate_aruco(charuco_board, aruco_ids, aruco_corners):
    print(
        "charuco_stereo_calibration.deduplicate_aruco: DO NOT USE IN NEW CODE; "
        "use charuco_utils's version instead."
    )
    return charuco_utils.deduplicate_aruco(charuco_board, aruco_ids, aruco_corners)


def is_corner_valid(board, corner_id, marker_ids, marker_corners):
    print(
        "charuco_stereo_calibration.is_corner_valid: DO NOT USE IN NEW CODE; "
        "use charuco_utils's version instead."
    )
    return charuco_utils.is_corner_valid(board, corner_id, marker_ids, marker_corners)


def setup_charuco_board(aruco_dict_name, columns, rows, checker_size, aruco_size):
    # type: (string, int, int, float, float) -> Tuple[cv2.aruco_Dictionary, cv2.aruco_CharucoBoard]
    """
    OBSOLETE. Always use get_charuco_board instead! This exists to support
    running get_calibration_{python, ros} with only board dimensions and a
    strong assumption about which dictionary and tags will be used.

    Helper function for getting paired dictionary + board.
    For boards that will be printed, define another option in get_charuco_board()
    """
    aruco_dictionary = cv2.aruco.getPredefinedDictionary(aruco_dict_name)
    charuco_board = cv2.aruco.CharucoBoard_create(
        columns, rows, checker_size, aruco_size, aruco_dictionary
    )
    return aruco_dictionary, charuco_board
