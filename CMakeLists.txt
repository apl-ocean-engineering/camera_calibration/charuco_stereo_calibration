cmake_minimum_required(VERSION 3.0.2)
project(charuco_stereo_calibration)

find_package(catkin REQUIRED COMPONENTS
  sensor_msgs
)

catkin_python_setup()

catkin_package()

catkin_install_python(PROGRAMS
  nodes/camera_info_spoofer.py
  scripts/run_calibration_ros.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
