#! /usr/bin/env python3
"""
Create the file that will be sent to the printers for V1 of the
localization columns.

The whitelisted aruco markers were chosen using jupyter notebook:
~~~
import cv2
import cv2.aruco
from plot_calibration import *

aruco_dict_name = cv2.aruco.DICT_4X4_250
aruco_dictionary = cv2.aruco.getPredefinedDictionary(aruco_dict_name)
plot_aruco_markers(aruco_dictionary, None, 10, 10)
~~~
Then select markers that have a corner in the middle and that aren't #17 =)

"""
# There are definite bugs in the save_board function that cause flake8 to raise errors.
# Not worth fixing 2 years after this script was last used.
# flake8: noqa

import argparse

import cv2
import cv2.aruco

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np

import os


class ColumnCreator(object):
    def __init__(self, tag_width, width, height, output_dir):
        self.tag_width = tag_width
        self.width = width
        self.height = height
        self.output_dir = output_dir

        self.aruco_dictionary = cv2.aruco.getPredefinedDictionary(
            cv2.aruco.DICT_4X4_250
        )
        self.good_markers = [0, 3, 4, 5, 6, 7, 8, 9,
                             11, 12, 13, 14, 15, 19,
                             21, 23, 24, 25, 26, 28, 29,
                             30, 31, 32, 33, 34, 35, 38,
                             42, 43, 44, 46, 47, 48,
                             51, 56, 58,
                             60, 62, 63, 65, 66, 67, 68, 69,
                             70, 72, 73, 74, 75, 76, 78, 79,
                             80, 81, 82, 83, 84, 86, 87, 88,
                             91, 92, 94, 95, 96, 97, 99]  # fmt: skip
        try:
            os.makedirs(self.output_dir)
        except FileExistsError:
            pass

    def run(self, num_columns):
        if 3 * num_columns > len(self.good_markers):
            msg = "Requested {} columns, but only have {} whitelisted tags".format(
                num_columns, len(self.good_markers)
            )
            raise Exception(msg)

        for cc in range(num_columns):
            filename = "{}/20200311_column{:02d}.pdf".format(self.output_dir, cc)
            id1 = self.good_markers[3 * cc]
            id2 = self.good_markers[3 * cc + 1]
            id3 = self.good_markers[3 * cc + 2]
            self.make_column(id1, id2, id3, filename, cc)

    def make_column(self, id1, id2, id3, filename, col_id):
        """
        Make a column using the specified tags.
        """
        dpi = 300
        num_tag_pixels = int(np.round(dpi * self.tag_width))

        fig = plt.figure(figsize=[self.width, self.height])

        xmargin_in = 0.5 * (self.width - self.tag_width)
        x0 = xmargin_in / self.width
        dx = self.tag_width / self.width

        ymargin_in = 2.0  # Enough space for the drill holes
        y1 = ymargin_in / self.height
        y2 = (0.5 * (self.height - self.tag_width)) / self.height
        y3 = (self.height - ymargin_in - self.tag_width) / self.height
        dy = self.tag_width / self.height

        for y0, charuco_id in zip([y1, y2, y3], [id1, id2, id3]):
            img = self.aruco_dictionary.drawMarker(charuco_id, num_tag_pixels)
            print("Adding tag {} at {}".format(charuco_id, (x0, y0)))
            ax = fig.add_axes([x0, y0, dx, dy])
            ax.set_xlabel("tag: {}".format(charuco_id), color="#cccccc", fontsize=18)
            for side in ["right", "left", "top", "bottom"]:
                ax.spines[side].set_visible(False)
            ax.set_xticks([])
            ax.set_yticks([])
            ax.imshow(img, cmap="gray", aspect="equal", interpolation="none")

        # TODO: Add drill marks for Jessica

        # Unlike before, these are in INCHES, not decimal position along axis
        # This is needed to get an actual circle =)
        drill_x = 0.5 * self.width
        drill_y1 = 0.5
        drill_y2 = self.height - 0.5
        drill_radius = 0.3

        drill_ax = fig.add_axes([0, 0, 1, 1])
        drill_ax.axis("off")

        for drill_y in [drill_y1, drill_y2]:
            drill_ax.plot(drill_x, drill_y, "rx", transform=fig.dpi_scale_trans)
            circ = mpatches.Circle(
                (drill_x, drill_y),
                drill_radius,
                transform=fig.dpi_scale_trans,
                edgecolor="r",
                facecolor="none",
            )
            drill_ax.add_patch(circ)

        drill_ax.text(
            0.5,
            0.5,
            '{} \n{:.02f}" pixels'.format(filename.split("/")[1], self.tag_width / 6.0),
            transform=fig.dpi_scale_trans,
            fontsize=18,
            color="#cccccc",
        )

        plt.savefig(filename)


def save_board():
    # Add drill markers around the border, 0.75" from the edge to match the 80/20
    drill_x1 = 0.75
    drill_x2 = fig_width - 0.75
    drill_y1 = 0.75
    drill_y2 = fig_height - 0.75
    # Want 4 holes along height, 5 along width
    n_vert_holes = 5
    n_horiz_holes = 5
    dx = (drill_x2 - drill_x1) / (n_horiz_holes - 1)
    dy = (drill_y2 - drill_y1) / (n_vert_holes - 1)

    # Top/bottom rows
    drill_coords = [(xx, drill_y1) for xx in np.arange(drill_x1, drill_x2 + 0.01, dx)]
    drill_coords.extend(
        [(xx, drill_y2) for xx in np.arange(drill_x1, drill_x2 + 0.01, dx)]
    )
    # sides
    drill_coords.extend(
        [(drill_x1, yy) for yy in np.arange(drill_y1 + dy, drill_y2, dy)]
    )
    drill_coords.extend(
        [(drill_x2, yy) for yy in np.arange(drill_y1 + dy, drill_y2, dy)]
    )

    print(drill_coords)

    drill_ax = fig.add_axes([0, 0, 1, 1])
    drill_ax.axis("off")

    for xx, yy in drill_coords:
        drill_ax.plot(xx, yy, "r.", transform=fig.dpi_scale_trans)

        # For 1/4-20 bolts, we want 1/4 + 1/32 = 9/32" diameter
        circ = mpatches.Circle(
            (xx, yy),
            9.0 / 64,
            transform=fig.dpi_scale_trans,
            edgecolor="r",
            facecolor="none",
        )
        drill_ax.add_patch(circ)

    drill_ax.text(
        1.5,
        0.5,
        board_name,
        transform=fig.dpi_scale_trans,
        fontsize=18,
        verticalalignment="center",
        horizontalalignment="left",
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "tag_width", type=float, help="Width of tag (6 pixels), in inches"
    )
    parser.add_argument("board_width", type=float, help="Width of the board")
    parser.add_argument("board_height", type=float, help="Height of the board")
    parser.add_argument("output_dir", help="Destination directory for images")
    parser.add_argument("num_columns", type=int, help="Number of columns to create")
    args = parser.parse_args()
    cc = ColumnCreator(
        args.tag_width, args.board_width, args.board_height, args.output_dir
    )
    cc.run(args.num_columns)
