#! /usr/bin/env python3

"""
Create set of aruco tags for use in our planned tank-positioning system.
* Uses the 4x4 aruco dictionary, which means the tags are 6 pixels wide
  (They have a bounding border of black)
* Output will be PDFs of specified size, one per side.
* The output PDF is 300 dpi, and includes a white border (of configurable width)

Only generates PDFs for tags that have been whitelisted
(some are noticeably worse than others.)
"""


import matplotlib.pyplot as plt
import os

import cv2
import cv2.aruco

import charuco_utils


def make_tags(side_length, border_width, ntags, output_dir):
    # Same dictionary and whitelist as hardcoded in calibration.py
    # TODO: copy-paste is ugly, but this serves the purpose of making these
    #       single-use scripts be more self contained.
    # (Not that it really matters, since the output image name will
    # include the tag's ID)
    aruco_dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_250)
    tag_ids = [
        ii
        for ii in range(len(aruco_dictionary.bytesList))
        if (
            charuco_utils.is_good_marker(aruco_dictionary, ii)
            and ii not in charuco_utils._fiducial_ids
            and ii not in charuco_utils._plinth_ids
        )
    ]
    if ntags > len(tag_ids):
        print(
            "Warning: Requested {} tags, but only have {} in whitelist".format(
                ntags, len(tag_ids)
            )
        )

    try:
        os.makedirs(output_dir)
    except FileExistsError:
        pass

    count = 0
    for idx in tag_ids:
        count += 1

        filename = "{}/aruco_{:03d}_{}in.pdf".format(output_dir, idx, side_length)
        print_tag(aruco_dictionary, idx, side_length, border_width, filename)

        if count >= ntags:
            break


def print_tag(aruco_dictionary, idx, side_length, margin_in, filename):
    """
    aruco_dictionary: cv2.aruco_Dictionary
    idx: tag ID
    side_length: tag size in inches
    filename: output filename
    """
    dpi = 300
    npixels = int(dpi * side_length)
    img = aruco_dictionary.drawMarker(idx, npixels)

    fig_width = side_length + 2 * margin_in
    fig_height = side_length + 2 * margin_in
    fig = plt.figure(figsize=[fig_width, fig_height], dpi=dpi)
    x0 = margin_in / (side_length + 2 * margin_in)
    y0 = x0
    dx = side_length / (side_length + 2 * margin_in)
    dy = dx
    ax = fig.add_axes([x0, y0, dx, dy])
    ax.axis("off")
    ax.imshow(img, cmap="gray", aspect="equal", interpolation="none")

    drill_ax = fig.add_axes([0, 0, 1, 1])
    drill_ax.axis("off")

    drill_ax.text(
        0.5 * (side_length + 2 * margin_in),
        0.25,
        "cv2.aruco.DICT_4X4_250   tag: {:03d}".format(idx),
        color=(0.7, 0.7, 0.7),
        transform=fig.dpi_scale_trans,
        fontsize=12,
        verticalalignment="center",
        horizontalalignment="center",
    )
    plt.savefig(filename)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("side_inches", type=float, help="Side length of tag")
    parser.add_argument("border_inches", type=float, help="Border width around tag")
    parser.add_argument("ntags", type=int, help="Number of tags to save")
    parser.add_argument("output_dir", help="Destination directory for images")
    args = parser.parse_args()
    make_tags(args.side_inches, args.border_inches, args.ntags, args.output_dir)
