#! /usr/bin/env python

# Author: Laura Lindzey & Mitchell Scott
# Copyright 2021-2022 University of Washington Applied Physics Laboratory
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.

# Standalone version of run_calibration_ros.py, without ROS dependencies

import cv2
import argparse
import os
import pickle
import glob

from charuco_stereo_calibration import calibration
from charuco_stereo_calibration import plot_calibration


class Calibrator:
    def __init__(self, args):
        if args.figure_dir != " ":
            self.figure_dir = args.figure_dir
        else:
            self.figure_dir = None
        self.base_dir = args.base_dir
        self.directory = args.directory
        if self.directory[-1] != "/":
            self.directory += "/"
        self.encoding = args.encoding
        self.stereo = args.stereo
        if self.stereo:
            self.image_index = None
            self.left_image_index = args.left_image_index
            self.right_image_index = args.right_image_index
        else:
            self.image_index = args.image_index
            self.left_image_index = None
            self.right_image_index = None
        self.checker_size = args.checker_size
        self.columns = args.columns
        self.rows = args.rows
        self.display_board = args.display_board

        self.min_corners = args.min_corners
        self.recalculate_index = args.recalculate_index

        self.save_debug_images = args.save_debug_images
        self.write_yaml_v1 = args.write_yaml_v1

        self.aruco_dictionary = None
        self.charuco_board = None

        self.order_utc = args.order_utc

        self.generate_board()

        if self.stereo:
            (
                self.image_size,
                self.left_index,
                self.right_index,
            ) = self.generate_stereo_indices()
        else:
            self.image_size, self.mono_index = self.generate_monocular_index()

    def run_calibration(self):
        self.run_intrinsic_calibration()
        if self.stereo:
            self.run_stereo_calibration()

    def run_intrinsic_calibration(self):
        if self.stereo:
            # TODO add back 'bad sequences'
            self._calibrate_intrinsic(self.left_index, [], "left", "stereo_left")
            self._calibrate_intrinsic(self.right_index, [], "right", "stereo_right")
        else:
            self._calibrate_intrinsic(self.mono_index, [], "mono", "camera_mono")

    def run_stereo_calibration(self):
        # rospy.get_param("~num_stereo_images")
        num_images = min(len(self.left_index), len(self.right_index))
        # rospy.get_param("~min_corners")
        min_corners = self.min_corners
        max_dt = 0.01  # seconds
        stereo_seqs = calibration.select_stereo_evenly_spaced(
            self.left_index, self.right_index, num_images, min_corners, max_dt
        )
        # print(stereo_seqs)

        left_stereo_filename = "{}/left_stereo_params.yaml".format(self.base_dir)
        right_stereo_filename = "{}/right_stereo_params.yaml".format(self.base_dir)

        # TODO: Logic for formatting filename is duplicated with _calibrate_intrinsic
        left_intrinsic_filename = "{}/left_calibration_params.yaml".format(
            self.base_dir
        )
        right_intrinsic_filename = "{}/right_calibration_params.yaml".format(
            self.base_dir
        )
        perspective_filename = "{}/perspective.yaml".format(self.base_dir)
        left_params = calibration.CalibrationParams(
            left_intrinsic_filename, write_yaml_v1=self.write_yaml_v1
        )
        right_params = calibration.CalibrationParams(
            right_intrinsic_filename, write_yaml_v1=self.write_yaml_v1
        )

        err = calibration.calibrate_stereo(
            self.charuco_board,
            stereo_seqs,
            self.image_size,
            self.left_index,
            self.right_index,
            left_params,
            right_params,
            left_stereo_filename,
            right_stereo_filename,
            perspective_filename=perspective_filename,
            write_yaml_v1=self.write_yaml_v1,
        )

        print("Calibrated stereo. Reprojection error = {}".format(err))

    def generate_stereo_indices(self):
        left_index_path = self.directory + "left/"
        right_index_path = self.directory + "right/"
        left_img_size, left_index = self._generate_index(
            self.left_image_index, left_index_path, dirname="left"
        )
        if self.display_board:
            for id in left_index:
                plot_calibration.plot_image_detections(left_index[id], show=True)
        right_img_size, right_index = self._generate_index(
            self.right_image_index, right_index_path, dirname="right"
        )
        if self.display_board:
            for id in right_index:
                plot_calibration.plot_image_detections(right_index[id], show=True)
        return left_img_size, left_index, right_index

    def generate_monocular_index(self):
        index_path = self.directory
        img_size, index = self._generate_index(self.image_index, index_path, dirname="")

        return img_size, index

    def generate_board(self):
        # In this dictionary, each white square has:
        # * 1 pixel white border
        # * 1 pixel black border (counts as part of the aruco marker size)
        # * 4x4 pixel aruco marker
        # TODO: If we start using other dictionaries, make these parameters!
        aruco_dict_name = cv2.aruco.DICT_4X4_250
        aruco_size = 0.75 * self.checker_size

        self.aruco_dictionary, self.charuco_board = calibration.setup_charuco_board(
            aruco_dict_name, self.rows, self.columns, self.checker_size, aruco_size
        )

        if self.figure_dir is not None:
            if not os.path.isdir(self.figure_dir):
                os.makedirs(self.figure_dir)
            plot_calibration.plot_aruco_markers(
                self.aruco_dictionary, self.figure_dir, 5, 10
            )
            plot_calibration.plot_charuco_board(self.charuco_board, self.figure_dir)

    def _generate_index(self, index_filename, dirpath, dirname="mono"):
        if os.path.isfile(index_filename) and not self.recalculate_index:
            print("Loading filename: {}".format(index_filename))
            index = pickle.load(open(index_filename, "rb"))
            img_size = self._get_imgsize_dir()
        else:
            print("generate index for {}, may take a while".format(dirname))
            index, img_size = calibration.generate_image_index_directory(
                self.aruco_dictionary,
                self.charuco_board,
                dirpath,
                encoding=self.encoding,
                order_utc=self.order_utc,
            )
            pickle.dump(index, open(index_filename, "wb"))

        return img_size, index

    def _get_imgsize_dir(self):
        if self.stereo:
            fnames = glob.glob(self.directory + "left/*" + self.encoding)
        else:
            fnames = glob.glob(self.directory + "*" + self.encoding)
        img = cv2.imread(fnames[0])
        return (img.shape[1], img.shape[0])

    def _calibrate_intrinsic(self, image_index, bad_seqs, prefix, camera_name):
        # rospy.get_param("~num_intrinsic_images")
        # Selecting all iamges
        num_images = len(image_index)
        min_corners = self.min_corners

        seqs = calibration.select_evenly_spaced(
            image_index, num_images, min_corners, bad_seqs
        )
        # print(seqs)
        if self.figure_dir is not None:
            fig_filename = "{}/{}_pose_distribution.png".format(self.figure_dir, prefix)
            plot_calibration.plot_pose_distributions(
                image_index, [("c", seqs)], filename=fig_filename
            )

        print("Calibrating mono cameras")
        output = calibration.calibrate_mono(
            self.charuco_board,
            self.image_size,
            image_index,
            seqs,
            display=self.display_board,
        )

        if output:
            err, ii, dd, _, _, _, _, _ = output
            calib_filename = "{}/{}_calibration_params.yaml".format(
                self.base_dir, prefix
            )
            if not os.path.isdir(self.base_dir):
                os.mkdir(self.base_dir)
            calibration.save_calibration_mono(
                calib_filename,
                camera_name,
                dd,
                ii,
                self.image_size,
                write_yaml_v1=self.write_yaml_v1,
            )

            _, _, _, _, _, _, _, per_view_errors = output

            print("")
            print("Intrinsic calibration errors for {}:".format(camera_name))
            if self.save_debug_images:
                print("  average reprojection error: {}".format(err))
                print("  Images with reprojection error > 1.0:")
                for error, seq in zip(per_view_errors, seqs):
                    filename = image_index[seq].image_filepath.split("/")[-1]
                    if error > 1.0:
                        print("    error: {}, image: {}".format(error, filename))
                    figname = "{}_image_{}_error_{}.png".format(
                        prefix, seq, int(100 * error)
                    )
                    # print("HERE")
                    print("       saving debug image to : {}".format(figname))
                    plot_calibration.plot_image_detections(
                        image_index[seq], "{}/{}".format(self.figure_dir, figname)
                    )
        else:
            print("No valid images found!")
            exit()


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Charuco calibration python standalone")

    parser.add_argument(
        "directory",
        help="Directory with images. If stereo, should point to parent directory with \
        a left/ and right/ directory below. If mono, should point to directory with images",
    )
    parser.add_argument(
        "--figure_dir",
        "-fd",
        default="../figs",
        help="Save figures (set empty string to ignore i.e., ' ' ",
    )
    parser.add_argument(
        "--base_dir", "-bd", default="../output", help="Output directory"
    )
    parser.add_argument("--encoding", "-e", default=".png", help="Image encoding")
    parser.add_argument("--stereo", action="store_true", help="Do stereo processing")
    parser.add_argument(
        "--checker_size",
        "-cs",
        default=0.045,
        help="Size of a checkerboard square",
        type=float,
    )
    parser.add_argument(
        "--columns", default=8, help="Size of a checkerboard cols", type=int
    )
    parser.add_argument(
        "--rows", default=11, help="Size of a checkerboard rows", type=int
    )
    parser.add_argument(
        "--image_index",
        default="image_index.pkl",
        help="Index for left images, if mono",
    )
    parser.add_argument(
        "--left_image_index",
        default="left_image_index.pkl",
        help="Index for left images, if stereo",
    )
    parser.add_argument(
        "--right_image_index",
        default="right_image_index.pkl",
        help="Index for right images, if stereo",
    )
    parser.add_argument(
        "--min_corners", "-mc", default=20, help="Min charuco corners", type=int
    )
    parser.add_argument(
        "--recalculate_index",
        "-rc",
        action="store_true",
        help="Will force index recalculation",
    )
    parser.add_argument(
        "--display_board",
        "-db",
        action="store_true",
        help="Display board during intrinsic calibration",
    )
    parser.add_argument(
        "--save_debug_images", action="store_true", help="Flag to save debugging images"
    )
    parser.add_argument(
        "--order_utc",
        action="store_true",
        help="Order input images by UTC time (see calibration.py get_directory_images_UTC for details",
    )
    parser.add_argument(
        "--write_yaml_v1",
        action="store_true",
        help="If true, will write data using YAML V1 format."
        + " This includes and updated header, writing the data type, and specifying if a data structure is an opencv or not."
        + " Required for reading with opencv Filesystem, but not the correct format for ROS. Default: False",
    )
    # parser.add_argument("--save_error_images", type=bool, default=False,
    #     help="Write debug images")
    args = parser.parse_args()

    calibrator = Calibrator(args)
    calibrator.run_calibration()
