#! /usr/bin/env python3

"""
Create set of aruco tags for use in our planned tank-positioning system.
* Uses the 4x4 aruco dictionary, which means the tags are 6 pixels wide
  (They have a bounding border of black)
* Output will be PDFs of specified size, one per side.
* The output PDF is 300 dpi, and includes a white border (of configurable width)

Only generates PDFs for tags that have been whitelisted
(some are noticeably worse than others.)
"""


import matplotlib.pyplot as plt
import os

import cv2
import cv2.aruco


def make_tags(side_length, border_width, ntags, output_dir):
    # Same dictionary and whitelist as hardcoded in calibration.py
    # TODO: copy-paste is ugly, but this serves the purpose of making these
    #       single-use scripts be more self contained.
    # (Not that it really matters, since the output image name will
    # include the tag's ID)
    aruco_dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_250)
    good_markers = [0, 3, 4, 5, 6, 7, 8, 9,
                    11, 12, 13, 14, 15, 19,
                    21, 23, 24, 25, 26, 28, 29,
                    30, 31, 32, 33, 34, 35, 38,
                    42, 43, 44, 46, 47, 48,
                    51, 56, 58,
                    60, 62, 63, 65, 66, 67, 68, 69,
                    70, 72, 73, 74, 75, 76, 78, 79,
                    80, 81, 82, 83, 84, 86, 87, 88,
                    91, 92, 94, 95, 96, 97, 99]  # fmt: skip
    if ntags > len(good_markers):
        # interactive_charuco_target_design.ipynb can be used to plot more
        # tags and identify ones that should be whitelisted.
        print(
            "Warning: Requested {} tags, but only have {} in whitelist".format(
                ntags, len(good_markers)
            )
        )

    try:
        os.makedirs(output_dir)
    except FileExistsError:
        pass

    count = 0
    for idx in good_markers:
        count += 1

        filename = "{}/aruco_{:03d}_{}in.pdf".format(output_dir, idx, side_length)
        print_tag(aruco_dictionary, idx, side_length, border_width, filename)

        if count >= ntags:
            break


def print_tag(aruco_dictionary, idx, side_length, margin_in, filename):
    """
    aruco_dictionary: cv2.aruco_Dictionary
    idx: tag ID
    side_length: tag size in inches
    filename: output filename
    """
    dpi = 300
    npixels = int(dpi * side_length)
    img = aruco_dictionary.drawMarker(idx, npixels)
    fig_width = side_length + 2 * margin_in
    fig_height = side_length + 2 * margin_in
    fig = plt.figure(figsize=[fig_width, fig_height], dpi=dpi)
    x0 = margin_in / (side_length + 2 * margin_in)
    y0 = x0
    dx = side_length / (side_length + 2 * margin_in)
    dy = dx
    ax = fig.add_axes([x0, y0, dx, dy])
    ax.axis("off")
    ax.imshow(img, cmap="gray", aspect="equal", interpolation="none")

    plt.savefig(filename)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("side_inches", type=float, help="Side length of tag")
    parser.add_argument("border_inches", type=float, help="Border width around tag")
    parser.add_argument("ntags", type=int, help="Number of tags to save")
    parser.add_argument("output_dir", help="Destination directory for images")
    args = parser.parse_args()
    make_tags(args.side_inches, args.border_inches, args.ntags, args.output_dir)
