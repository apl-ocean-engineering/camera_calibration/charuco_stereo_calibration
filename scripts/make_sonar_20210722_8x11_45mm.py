#! /usr/bin/env python3

"""
Create the file that will be sent to the printers for V2 of the
sonar-specialized charuco boards.
The only difference between this and make_charuco_20210722_8x11_45mm
is the addition of red crosses in the midddle of black squares, to make it
easier to drill in the exact center.

The whitelisted aruco markers were chosen using calibration.is_good_marker
and avoiding the IDs already used in fiducials
~~~
plinth_ids = []
for tag_id in range(len(aruco_dictionary.bytesList)):
    # Go ahead and reserve a few extra early-ish ones for the wall tags
    if tag_id < 50:
        continue
    if len(plinth_ids) >= 70:
        break
    if tag_id in fiducial_ids:
        continue
    if not calibration.is_good_marker(aruco_dictionary, tag_id):
        continue
    plinth_ids.append(tag_id)
import cv2
import cv2.aruco
from plot_calibration import *
~~~

"""

import matplotlib.pyplot as plt

import charuco_stereo_calibration.calibration as calib


def save_board():
    board_name = "charuco_20210722_8x11_45mm"
    nrows, ncols, checker_size = calib.get_board_params(board_name)
    _, charuco_board = calib.get_charuco_board(board_name)

    # Use built-in utility to get board as an image
    inches_per_meter = 39.3701
    dpi = 300
    checker_pixels = int(checker_size * inches_per_meter * dpi)
    print("save_board, with checker_pixels = {}".format(checker_pixels))
    board_img = charuco_board.draw(
        (ncols * checker_pixels, nrows * checker_pixels), marginSize=0
    )

    fig_width = 24  # inches
    fig_height = 18
    fig = plt.figure(figsize=[fig_width, fig_height], dpi=dpi)

    # Center the board image in the figure
    x_margin = fig_width * dpi - board_img.shape[1]
    y_margin = fig_height * dpi - board_img.shape[0]
    x0 = 0.5 * x_margin / (fig_width * dpi)
    y0 = 0.5 * y_margin / (fig_height * dpi)
    dx = board_img.shape[1] / (fig_width * dpi)
    dy = board_img.shape[0] / (fig_height * dpi)
    print(
        "Checkerboard has a margin of {}, {} compared to board size".format(
            x_margin, y_margin
        )
    )
    print("resulting board axis: [{}, {}, {}, {}]".format(x0, y0, dx, dy))

    board_ax = fig.add_axes([x0, y0, dx, dy])
    board_ax.axis("off")
    board_ax.imshow(board_img, cmap="gray", aspect="equal", interpolation="none")

    # Add red dots in the middle of each black square, for easier sonar fiducial creation
    for row in range(nrows):
        if row % 2 == 1:
            cols = range(1, ncols, 2)
        else:
            cols = range(0, ncols, 2)
        for col in cols:
            xx = (col + 0.5) * checker_pixels
            yy = nrows * checker_pixels - (row + 0.5) * checker_pixels
            board_ax.plot(xx, yy, "rx", ms=3, markeredgewidth=0.2)

    drill_ax = fig.add_axes([0, 0, 1, 1])
    drill_ax.axis("off")

    drill_ax.text(
        1.5,
        0.5,
        board_name,
        transform=fig.dpi_scale_trans,
        fontsize=18,
        verticalalignment="center",
        horizontalalignment="left",
    )

    plt.savefig("sonar_{}.pdf".format(board_name))


if __name__ == "__main__":
    save_board()
