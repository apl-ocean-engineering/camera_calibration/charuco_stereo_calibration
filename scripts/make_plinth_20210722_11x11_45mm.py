#! /usr/bin/env python3

"""
Create the file that will be sent to the printers for V2 of the plinth boards.

These are meant to be suitable for the Cat and Dog statues.

Mr. Easter has a 11 x 11.5" footprint, and requires a 22x22" board with 45mm
squares in order to have two full rows visible around its outline.

(goes beyond plot_calibration.plot_charuco_board by adding drill markers)

The whitelisted aruco markers were chosen using calibration.is_good_marker
and avoiding the IDs already used in fiducials
~~~
plinth_ids = []
for tag_id in range(len(aruco_dictionary.bytesList)):
    # Go ahead and reserve a few extra early-ish ones for the wall tags
    if tag_id < 50:
        continue
    if len(plinth_ids) >= 70:
        break
    if tag_id in fiducial_ids:
        continue
    if not calibration.is_good_marker(aruco_dictionary, tag_id):
        continue
    plinth_ids.append(tag_id)
import cv2
import cv2.aruco
from plot_calibration import *
~~~

"""

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np

import charuco_stereo_calibration.calibration as calib


def save_board():
    board_name = "plinth_20210722_11x11_45mm"
    nrows, ncols, checker_size = calib.get_board_params(board_name)
    _, charuco_board = calib.get_charuco_board(board_name)

    # Use built-in utility to get board as an image
    inches_per_meter = 39.3701
    dpi = 300
    checker_pixels = int(checker_size * inches_per_meter * dpi)
    print("save_board, with checker_pixels = {}".format(checker_pixels))
    board_img = charuco_board.draw(
        (ncols * checker_pixels, nrows * checker_pixels), marginSize=0
    )

    fig_width = 22  # inches
    fig_height = 22
    fig = plt.figure(figsize=[fig_width, fig_height], dpi=dpi)

    # Center the board image in the figure
    x_margin = fig_width * dpi - board_img.shape[1]
    y_margin = fig_height * dpi - board_img.shape[0]
    x0 = 0.5 * x_margin / (fig_width * dpi)
    y0 = 0.5 * y_margin / (fig_height * dpi)
    dx = board_img.shape[1] / (fig_width * dpi)
    dy = board_img.shape[0] / (fig_height * dpi)
    print(
        "Checkerboard has a margin of {}, {} compared to board size".format(
            x_margin, y_margin
        )
    )
    print("resulting board axis: [{}, {}, {}, {}]".format(x0, y0, dx, dy))

    board_ax = fig.add_axes([x0, y0, dx, dy])
    board_ax.axis("off")
    board_ax.imshow(board_img, cmap="gray", aspect="equal", interpolation="none")

    # Add drill markers around the border, 0.75" from the edge to match the 80/20
    drill_x1 = 0.75
    drill_x2 = fig_width - 0.75
    drill_y1 = 0.75
    drill_y2 = fig_height - 0.75
    # Want 4 holes along height, 5 along width
    n_vert_holes = 5
    n_horiz_holes = 5
    dx = (drill_x2 - drill_x1) / (n_horiz_holes - 1)
    dy = (drill_y2 - drill_y1) / (n_vert_holes - 1)

    # Top/bottom rows
    drill_coords = [(xx, drill_y1) for xx in np.arange(drill_x1, drill_x2 + 0.01, dx)]
    drill_coords.extend(
        [(xx, drill_y2) for xx in np.arange(drill_x1, drill_x2 + 0.01, dx)]
    )
    # sides
    drill_coords.extend(
        [(drill_x1, yy) for yy in np.arange(drill_y1 + dy, drill_y2, dy)]
    )
    drill_coords.extend(
        [(drill_x2, yy) for yy in np.arange(drill_y1 + dy, drill_y2, dy)]
    )

    print(drill_coords)

    drill_ax = fig.add_axes([0, 0, 1, 1])
    drill_ax.axis("off")

    for xx, yy in drill_coords:
        drill_ax.plot(xx, yy, "r.", transform=fig.dpi_scale_trans)

        # For 1/4-20 bolts, we want 1/4 + 1/32 = 9/32" diameter
        circ = mpatches.Circle(
            (xx, yy),
            9.0 / 64,
            transform=fig.dpi_scale_trans,
            edgecolor="r",
            facecolor="none",
        )
        drill_ax.add_patch(circ)

    drill_ax.text(
        1.5,
        0.5,
        board_name,
        transform=fig.dpi_scale_trans,
        fontsize=18,
        verticalalignment="center",
        horizontalalignment="left",
    )

    plt.savefig("{}.pdf".format(board_name))


if __name__ == "__main__":
    save_board()
