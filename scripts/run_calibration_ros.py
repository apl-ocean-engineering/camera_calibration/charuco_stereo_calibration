#! /usr/bin/env python

# Author: Laura Lindzey
# Copyright 2020-2022 University of Washington Applied Physics Laboratory
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.

# Script to calibrate cameras using bagfile containing calibration dance.
# Based on CameraCalibrationPipeline.ipynb and StereoCalibration.ipynb

from __future__ import print_function

import os
import pickle

import cv2
import cv2.aruco

import rospy
from collections import namedtuple

from charuco_stereo_calibration import calibration
from charuco_stereo_calibration import plot_calibration

CalibrationData = namedtuple("CalibrationData", ["index", "image_size"])


class CharucoIntrinsicCalibrator(object):
    def __init__(self):
        # If not None, a number of the steps will save debug figures
        self.figure_dir = rospy.get_param("~figdir")
        self.meta_dir = rospy.get_param("~metadir")

        self.generate_board()

        # Generate image indices
        self.bagdir = rospy.get_param("~bagdir")
        self.stereo = rospy.get_param("~stereo")  # bool

        if self.stereo:
            self.left_data, self.right_data = self.generate_stereo_indices()
        else:
            self.image_data = self.generate_monocular_index()
            print("Generated index, image size = {}".format(self.image_data.image_size))

    def run_calibration(self):
        self.run_intrinsic_calibration()
        if self.stereo:
            self.run_stereo_calibration()

    def generate_board(self):
        # Generate aruco dictionary / charuco board
        ncols = rospy.get_param("~checkerboard_columns")
        nrows = rospy.get_param("~checkerboard_rows")
        checker_size = rospy.get_param("~checker_size")

        # In this dictionary, each white square has:
        # * 1 pixel white border
        # * 1 pixel black border (counts as part of the aruco marker size)
        # * 4x4 pixel aruco marker
        # TODO: If we start using other dictionaries, make these parameters!
        aruco_dict_name = cv2.aruco.DICT_4X4_250
        aruco_size = 0.75 * checker_size

        self.aruco_dictionary, self.charuco_board = calibration.setup_charuco_board(
            aruco_dict_name, ncols, nrows, checker_size, aruco_size
        )

        if self.figure_dir is not None:
            if not os.path.isdir(self.figure_dir):
                os.makedirs(self.figure_dir)
            plot_calibration.plot_aruco_markers(
                self.aruco_dictionary, self.figure_dir, 5, 10
            )
            plot_calibration.plot_charuco_board(self.charuco_board, self.figure_dir)

    def generate_monocular_index(self):
        """
        Load or generate index for a single camera.
        """
        mono_index_filename = rospy.get_param("~mono_index_filename")
        print(
            "\nGenerating monocular index. Filename: \n * {}\n".format(
                mono_index_filename
            )
        )
        mono_topic = rospy.get_param("~mono_topic")
        calib_data = self._generate_index(
            self.bagdir, mono_index_filename, mono_topic, "mono"
        )
        return calib_data

    def generate_stereo_indices(self):
        """
        Load or generate index for both cameras in a stereo pair.
        """
        left_index_filename = rospy.get_param("~left_index_filename")
        right_index_filename = rospy.get_param("~right_index_filename")
        print(
            "\nGenerating stereo index. Filenames: \n * {}\n * {}\n".format(
                left_index_filename, right_index_filename
            )
        )

        left_topic = rospy.get_param("~left_topic")
        right_topic = rospy.get_param("~right_topic")

        left_data = self._generate_index(
            self.bagdir, left_index_filename, left_topic, "left"
        )
        right_data = self._generate_index(
            self.bagdir, right_index_filename, right_topic, "right"
        )

        return left_data, right_data

    def _generate_index(self, bagdir, index_filename, image_topic, prefix):
        """
        Try loading image index; if it doesn't exist, create it.
        (helper function for generate_monocular_index, generate_stereo_indices)

        Generating the index and saving images can take over an hour, so it
        should only be done once.

        NOTE: Using pickle is a bit fragile -- I think it'll fail to load the
            index if I change how calibration is imported.

        @param bagdir: index will be generated of bagdir/*.bag
        @param index_filename: filename to load/save index as
        @param image_topic: index wil be of all sensor_msgs/Image messages
                            matching this topic
        @param prefix: directory prefix for saved images (within self.figure_dir)
        """
        try:
            index = pickle.load(open(index_filename, "rb"))
        except Exception as ex:
            print("unable to load {}".format(index_filename))
            print(ex)
            index = None

        if index is None or len(index) == 0:
            print("Regenerating index ... this may take a while")
            index = calibration.generate_image_index(
                bagdir,
                image_topic,
                self.aruco_dictionary,
                self.charuco_board,
                "{}/{}_images".format(self.figure_dir, prefix),
                save_images=True,
            )

            if index is None or len(index) == 0:
                print("Unable to generate index!")
                raise Exception(
                    "No image data, probably an error in image processing..."
                )

        pickle.dump(index, open(index_filename, "wb"))

        image_size = calibration.get_image_size(bagdir, image_topic)
        return CalibrationData(index, image_size)

    def run_intrinsic_calibration(self):
        if self.stereo:
            # For now, we manually inspect images with suspiciously high
            # reprojection errors.
            bad_left_seqs = rospy.get_param("~bad_left_seqs")
            bad_right_seqs = rospy.get_param("~bad_right_seqs")

            # TODO: Maybe camera name needs to be a parameter?
            left_name = rospy.get_param("~left_name", "left")
            right_name = rospy.get_param("~right_name", "right")

            self._calibrate_intrinsic(
                self.left_data, bad_left_seqs, left_name, "stereo_%s" % left_name
            )
            self._calibrate_intrinsic(
                self.right_data, bad_right_seqs, right_name, "stereo_%s" % right_name
            )
        else:
            bad_seqs = rospy.get_param("~bad_mono_seqs")
            camera_name = rospy.get_param("~camera_name")
            self._calibrate_intrinsic(
                self.image_data, bad_seqs, camera_name, "camera_%s" % camera_name
            )

    def _calibrate_intrinsic(self, calib_data, bad_seqs, prefix, camera_name):
        # NB: The filename-formatting logic is duplicated in run_stereo_calibration
        calib_filename = "{}/{}_intrinsic_params.yaml".format(self.meta_dir, prefix)

        if os.path.exists(calib_filename):
            print(
                "Skipping intrinsic calibration, since {} exists.".format(
                    calib_filename
                )
            )
            return

        num_images = rospy.get_param("~num_intrinsic_images")
        min_corners = rospy.get_param("~min_corners")

        seqs = calibration.select_evenly_spaced(
            calib_data.index, num_images, min_corners, bad_seqs
        )

        if self.figure_dir is not None:
            fig_filename = "{}/{}_pose_distribution.png".format(self.figure_dir, prefix)
            plot_calibration.plot_pose_distributions(
                calib_data.index, [("c", seqs)], filename=fig_filename
            )

        output = calibration.calibrate_mono(
            self.charuco_board, calib_data.image_size, calib_data.index, seqs
        )

        if output:
            err, ii, dd, _, _, _, _, _ = output
            calibration.save_calibration_mono(
                calib_filename, camera_name, dd, ii, calib_data.image_size
            )

            _, _, _, _, _, _, _, per_view_errors = output

            print("")
            print("Intrinsic calibration errors for {}:".format(camera_name))
            print("  average reprojection error: {}".format(err))
            print("  Images with reprojection error > 1.0:")
            for error, seq in zip(per_view_errors, seqs):
                filename = calib_data.index[seq].image_filepath.split("/")[-1]
                if error > 1.0:
                    print("    error: {}, image: {}".format(error, filename))
                figname = "{}_image_{:06d}_error_{:03d}.png".format(
                    prefix, seq, int(100 * error)
                )
                print("       saving debug image to : {}".format(figname))
                plot_calibration.plot_image_detections(
                    calib_data.index[seq], "{}/{}".format(self.figure_dir, figname)
                )
        else:
            print("No valid images found!")
            exit()

    def run_stereo_calibration(self):
        num_images = rospy.get_param("~num_stereo_images")
        min_corners = rospy.get_param("~min_corners")
        max_dt = 0.01  # seconds
        stereo_seqs = calibration.select_stereo_evenly_spaced(
            self.left_data.index, self.right_data.index, num_images, min_corners, max_dt
        )

        left_name = rospy.get_param("~left_name", "left")
        right_name = rospy.get_param("~right_name", "right")

        left_stereo_filename = "{}/{}_stereo_params.yaml".format(
            self.meta_dir, left_name
        )
        right_stereo_filename = "{}/{}_stereo_params.yaml".format(
            self.meta_dir, right_name
        )

        # TODO: Logic for formatting filename is duplicated with _calibrate_intrinsic
        left_intrinsic_filename = "{}/{}_intrinsic_params.yaml".format(
            self.meta_dir, left_name
        )
        right_intrinsic_filename = "{}/{}_intrinsic_params.yaml".format(
            self.meta_dir, right_name
        )
        transform_filename = "{}/{}_{}_transform.yaml".format(
            self.meta_dir, left_name, right_name
        )

        left_params = calibration.CalibrationParams(left_intrinsic_filename)
        right_params = calibration.CalibrationParams(right_intrinsic_filename)

        if self.left_data.image_size == self.right_data.image_size:
            err = calibration.calibrate_stereo(
                self.charuco_board,
                stereo_seqs,
                self.left_data.image_size,
                self.left_data.index,
                self.right_data.index,
                left_params,
                right_params,
                left_stereo_filename,
                right_stereo_filename,
                transform_filename=transform_filename,
            )

            print("Calibrated stereo. Reprojection error = {}".format(err))
        else:
            print("Left and right image sizes are different, doing limited calibration")
            err = calibration.calibrate_stereo(
                self.charuco_board,
                stereo_seqs,
                self.left_data.image_size,
                self.left_data.index,
                self.right_data.index,
                left_params,
                right_params,
                do_rectify=False,
                transform_filename=transform_filename,
            )


if __name__ == "__main__":
    rospy.init_node("run_calibration")

    calibrator = CharucoIntrinsicCalibrator()
    calibrator.run_calibration()
