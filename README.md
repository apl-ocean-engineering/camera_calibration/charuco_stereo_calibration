## Overview

Tools for using a charuco board to calibrate a stereo pair.
(This utility started out in the falcon_dalsa_calibration_2020-06-29_analysis repo)

For now, calibration is offline -- it generates an index of all available
images, then selects a subset of images to use in calibration.

scripts/run_calibration_ros.py provides a wrapper around the utilities in
src/charuco_stereo_calibration, properly handling the various ROS params,
sequencing the calibration steps, and generating debug images.
(run_calibration_python.py is a ROS-less version of the script.)

It should be run from an analysis repo -- example launch files can be found
in the charuco_calibration_analysis repo.


Outputs:

* Intrinsic parameters will be saved to ${metadir}/[left,right,mono]_calibration_params.yaml
* Extrinsic (stereo) calibration will be saved to ${metadir}/[left,right]_stereo_params.yaml

To test/inspect the calibration:

* nodes/camera_info_spoofer.py publishes the appropriate CameraInfo message for every Image message. Run `rosrun charuco_stereo_calibration camera_info_spoofer.py` from the directory containing the .yaml calibration files. (TODO: clean this node up use params rather than hardcoded filenames, and to support mono rather than just stereo)
* use camera_info_spoofer + stereo_image_proc + rviz to inspect the resulting pointcloud. Example launch file in charuco_calibration_analysis/2020-07-09_dalsa_camera_calibration/test_calibration.launch.

### Example
Your directory structure might look something like the following. You should have a catkin workspace (built using ```catkin build```), which contains a source folder, which will contain this repository alongside the other repository which contains the stereo data. Place the launch file in the same folder as the bag file which contains the stereo data.
```
project_ws
└───src
│   └───charuco_stereo_calibration
│   └───stereo_analysis
│   │   └───first_dataset
│   │   │   │   run_calibration.launch
│   │   │   │   stereo_data.bag
│   │   │   CMakeLists.txt
│   │   │   package.xml
└───logs
└───devel
│   │   setup.bash
└───build
```

An example launch file looks like this; make sure to tweak the parameters/ comment and uncomment lines for your use.
```xml
<launch>
  <!-- hacky way to give relative paths to output files -->
  <arg name="CWD" value="$(find stereo_analysis)/first_dataset"/>

  <node name="$(anon charuco_calibration)" type="run_calibration_ros.py" pkg="charuco_stereo_calibration" output="screen" args="--figdir $(arg CWD)/foo" required="true">

    <!-- Directory where metadata is saved (e.g. the index files) -->
    <param name="metadir" value="$(arg CWD)"/>
    <!-- directory where bagfiles are read from -->
    <param name="bagdir" value="$(arg CWD)"/>
    <!-- Directory where all figures will be saved -->
    <param name="figdir" value="$(arg CWD)/figures"/>

    <!-- These parameters match the dibond purchased from
         https://calib.io/pages/camera-calibration-pattern-generator -->
    <param name="checkerboard_columns" type="int" value="11"/>
    <param name="checkerboard_rows" type="int" value="8"/>
    <param name="checker_size" type="double" value="0.015"/>

    <!--
    if stereo=false, only generate one index file, and run intrinsic calibration.
    if stereo=true, will generate index files for each camera, run intrinsic calibration
       on both, and then run the extrinsic stereo calibration.
    -->
    <param name="stereo" type="bool" value="true"/>

    <param name="left_topic" value="/left/image_raw"/>
    <param name="right_topic" value="/right/image_raw"/>
    <param name="left_index_filename" value="$(arg CWD)/left_image_index.pkl"/>
    <param name="right_index_filename" value="$(arg CWD)/right_image_index.pkl"/>

    <!--
    <param name="mono_topic" value="/camera/image_mono"/>
    <param name="mono_index_filename" value="$(arg CWD)/mono_image_index.pkl"/>
    -->

    <!--
    Which sequence numbers to ignore.

    The calibration is not yet robust enough to determine images that are too
    blurry to have robust corner localization, or where mis-identified charuco
    tags have borked everything. So, for now, those are manually identified
    and excluded using this parameter.
    -->

    <param name="bad_left_seqs" type="yaml" value="[190]"/>
    <param name="bad_right_seqs" type="yaml" value="[]"/>

    <!--
    <param name="bad_mono_seqs" type="yaml" value="[]"/>
    -->

    <!-- how many images to use for intrinsic calibration -->
    <param name="num_intrinsic_images" type="int" value="100"/>
    <!-- how many images to use for stereo calibration -->
    <param name="num_stereo_images" type="int" value="10"/>
    <!-- how many corners to require in an image to use it for calibration -->
    <param name="min_corners" type="int" value="20"/>


  </node>

</launch>
```
Then, all you have to do is ```catkin build```, ```source devel/setup.bash```, and ```roslaunch src/stereo_analysis/first_dataset/run_calibration.launch```.
## Target generation

We are using charuco boards for the plints for doing object reconstruction.
These boards are not used in the calibration pipeline, but utilities for
generating them are version controlled in this repo.

So far, we have been adjusting:

* board size and checkerboard rows/cols
* aruco marker size
* which aruco markers to use (some are less reliable than others)

It is important that boards/plinths use different markers than the fiducials do.
To enforce this, we maintain two lists:
* plinth_ids -- white list of allowed markers, will be repeated on different plinths. If there aren't enough markers in the list for the desired plinth size, manually add more.
* fiducial_ids -- list of markers that have been used in manufactured fiducial tags. When new tags are ordered, they should be appended to this list (after checking that the pass calibration.is_god_marker AND aren't on the plinth_markers.txt list!)


Additionally, we require that any marker used on any printed fiducial/board have a clearly-defined center point, which means it is composed of an even number of squares per side, and that there's an identifiable corner in the middle (rather than a solid color, or a line).
`calibration.is_good_marker(...)` checks for this.

Each target should be assigned a unique name, which will be used to look
up its parameters. To add a new target:

* edit src/charuco_stereo_calibration/calibration.py to add "${NAME}" to `get_board_params` and `get_charuco_board`
* scripts/make_${NAME}.py -- standalone script using the calibration.py's utility functions to creates ${NAME}.pdf, which is a file suitable for sending to the printers. It often includes drill holes for mounting the target.
* Add ${NAME}.pdf to the repo (the images are small and won't be changed, and it's good to have a reference)

TODO: Consider separating data/configuration from code, and having new targets
    be defined in a yaml file.

Existing targets:
* default_11x8_45mm : V1 of the target, used for initial calibration + same as calib.io
* plinth_20201023_24x18 : V1 of the plinth, will be used for the mushroom anchor
* plinth_20201026_22x22 : V1 of the plinth, shaped for Mr. Easter.
* target_20210722_11x8_45mm : V2 of the target, using aruco markers that AREN'T duplicated in the fiducials
* plinth_20210722_22x22 : V2 of the plinth, using aruco markers not duplicated in the fiducials

## Calibration stages

The process is automated, but provides fairly verbose output for the operator
to inspect the different stages of calibration. Set the "figdir" parameter
in order for these images to be saved.

### Target generation

TODO: Update calibration pipeline to use target name, and version control
that target in the same way that plinth targets are handled.

Target generation is controlled by three parameters:

* checkerboard_columns: number of columns in the board
* checkerboard_rows: number of rows in the board
* checker_size: width of each checker, in meters

TODO: If we use a different aruco dict or different target size, the
    hardcoded values need to be replaced by parameters.

Two output figures are generated:

* ${figdir}/aruco_4x4_markers.png: aruco markers in the dictionary, with their labels
* ${figdir}/charuco_board.png:the charuco board generated from the input parameters


### Index generation

Reading all data from a bagfile can be very slow, and there aren't any
tools for quickly selecting a given message, so we do a pre-processing
step that identifies all images in the bagfile, computes the location of
the charuco board in the image, and then saves the image as a png.

TODO: Make the saving optional, since it eats up disk space and is only
    necessary for generating debug figures.

The index files will be saved in the directory specified by the
"metadir" parameter. If an index already exists in that directory,
it will be loaded, saving significant processing time.

NOTE: The index is saved using pickle, which seems rather fragile. I think
    that if the module name for charuco_stereo_calibration.calibration
    is changed, the pickle file will become invalid.


### Image selection

Currently, images are selected to be evenly-spaced in time, since that
worked well enough with the existing bagfiles. It would be interesting
to try to select them to be maximally distributed in checkerboard pose.

Relevant parameters:
* num_intrinsic_images: How many images to use in the calibration. In CameraCalibrationPipeline.ipynb, I started looking at the optimal number for this (looking at reprojection error + convergence time vs. number of images), but didn't get definitive results regarding reprojection error. In practice, 60 images takes ~15 seconds.
* num_corners: How many corners must be detected in an image to use it for calibration.
* bad_{left,right}_seqs: manually-identified images that should not be used for calibration

For each camera being calibrated, a figure will be generated showing ALL
charuco board poses in the index, along with the poses of the images that
will be used for calibration:

* ${figdir}/[left, right, mono]_pose_distribution.png


### Intrinsic Calibration

TODO: The camera name saved in the yaml file might need to be a param;
    for now, it's hard coded based on stereo/mono.

Pay attention to the script's printed output.
For any image with reprojection error > 1.0, a figure will be generated
showing the detected aruco markers and checkerboard corners. These should
be inspected, and any egregious ones should have their sequence number
added to the corresponding parameter.

### Stereo Calibration

Very little introspection available here -- this step massages the known
charuco points in the target frame + their observed locations in each camera
into the correct format to call cv2's calibration and rectification
functions.

Use rviz to inspect the resulting pointclouds.
